
var path = require('path')

module.exports = {
  build: {
    env: require('./prod.env'),
    index: path.resolve(__dirname, '../dist/index.html'),
    assetsRoot: path.resolve(__dirname, '../dist'),
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    productionSourceMap: true,
    
    productionGzip: false,
    productionGzipExtensions: ['js', 'css'],
    
    bundleAnalyzerReport: process.env.npm_config_report
  },
  dev: {
    env: require('./dev.env'),
    port: process.env.PORT,
    autoOpenBrowser: false,
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    proxyTable: {
      '/login': {
        target: process.env.AUTH_API_ADDRESS || 'http://127.0.0.1:8081',
        secure: false
      },
      '/todos': {
        target: process.env.TODOS_API_ADDRESS || 'http://127.0.0.1:8082',
        secure: false
      },
      '/zipkin': {
        target: process.env.ZIPKIN_URL || 'http://127.0.0.1:9411/api/v2/spans',
        pathRewrite: {
          '^/zipkin': ''
        },
        secure: false
      },      
    },
    
    cssSourceMap: false
  }
}
